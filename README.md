# Vagrant Django

This project can be used as a starting point for any Django project where
developers will collaborate and need a consistent development environment that
is easy to set up.

The intent is that any new developer does not need to install project
dependencies to their machine, but can simply clone this repository and run
`vagrant up` to have everything running in a virtual machine. Necessary project
files are synced between the VM and localhost and symlinked as necessary to the
proper system locations on the VM.

By keeping the VM provisioning script simple and avoiding additional overhead of
containerization, the learning curve for new team members is small. This means
fast and early contribution to the project and dead simple deployments to
production environments for the devops team as well.

## What's in the box?

- [`debian/contrib-stretch64`](https://app.vagrantup.com/debian/boxes/contrib-stretch64)
  virtual box OS (`contrib-*` boxes include the `vboxfs` kernel module for shared folders)
- [Python 3.5](https://www.python.org/)
- [Pipenv](https://pipenv.readthedocs.io/en/latest/)
- [Django](https://djangoproject.com/)
- [PostgreSQL](https://www.postgresql.org/)
- [nginx](https://nginx.org/en/)
- [OpenSSL](https://www.openssl.org/) generated development SSL certificate

## Set up
1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and
   [Vagrant](https://www.vagrantup.com/downloads.html).
1. Clone this repository and copy of the `.env` template into the project root:

        $ git clone git@<repository_host>:vagrant-django.git <project_name>
        $ cd <project_name>
        $ cp conf/template.env .env

1. Update `.env` with your project variables. Update the project name in
   `conf/provision.sh` to match.
1. Launch the development VM.

        $ vagrant up

1. View the running application in a browser at https://172.30.0.10.

## Development

You can get started with
[Django development](https://docs.djangoproject.com/en/dev/#the-development-process)
as soon as you ssh into the VM.

- Use `vagrant ssh` to access the VM shell
- [Pipenv](https://pypi.org/project/pipenv/) is already installed on the VM and
  ready to use. You do not need to install or use virtualenv/virtualenvwrapper
- Use `pipenv run` to run a given command from the virtualenv, with any
  arguments forwarded
- Use `pipenv install [OPTIONS] [PACKAGE_NAME]` to install additional packages
  in the virtualenv

For example, to start a new Django app, run:

    $ pipenv run python manage.py startapp <app_name>

### Environment variables

A `.env` file is created when you first call `vagrant up` and should be
edited to match your project specifications. Be sure to change the `SECRET_KEY`
value for any production-like environments.

### SSL in Development

In order to develop in a production-like as possible environment a self-signed
SSL certificate can be used. The dev key and certificate are already setup but
the steps to create them are documented below for reference.

To create a self-signed key and certificate pair, use the OpenSSL
[`req` command](https://www.openssl.org/docs/manmaster/man1/req.html) to create
the `.key` and `.crt` files.

    $ openssl req -x509 -nodes -days 365 -newkey rsa -config /vagrant/conf/ssl/dev.conf \
                                                     -keyout /vagrant/conf/ssl/dev.key \
                                                     -out /vagrant/conf/ssl/dev.crt
