from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from django.views.debug import default_urlconf


class ExampleTest(TestCase):

    def test_debug_default_urlconf_returns_welcome_page(self):
        request = HttpRequest()
        response_html = default_urlconf(request).content.decode('utf8')
        self.assertIn('<title>Django: the Web framework for perfectionists with deadlines.</title>', response_html)
        self.assertIn('<h2>The install worked successfully! Congratulations!</h2>', response_html)
