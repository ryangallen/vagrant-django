#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
PROJECT_NAME='vagrant_django_project'
DB_NAME=$PROJECT_NAME'_db'

sudo -E apt-get update
sudo -E apt-get install -y screen git nginx python3-pip python3-dev

python3 -m pip install --user pipenv

# Install PostgreSQL and create database
if ! command -v psql; then
    sudo -E apt-get install -y postgresql libpq-dev
    sudo su - postgres --command 'createuser -s vagrant'
    createdb $DB_NAME
fi

# Create development SSL key and certificate
openssl req -x509 -nodes -days 365 -newkey rsa -config /vagrant/conf/ssl/dev.conf \
                                               -keyout /vagrant/conf/ssl/dev.key \
                                               -out /vagrant/conf/ssl/dev.crt
sudo ln -sf /vagrant/conf/ssl/* /etc/ssl/.

# Set up nginx
sudo rm /etc/nginx/sites-enabled/*
sudo ln -sf /vagrant/conf/nginx/sites-available/* /etc/nginx/sites-enabled/.
sudo ln -sfd /vagrant/.media /var/media
sudo ln -sfd /vagrant/.static /var/static
sudo rm -r /var/www && sudo ln -sfd /vagrant/conf/nginx/try_files /var/www
sudo systemctl start nginx

# Set up logging directory
sudo mkdir -p /var/log/django
sudo chown -R vagrant /var/log/django
sudo chmod -R 775 /var/log/django

# Set up environment
if [ ! -f /vagrant/.env ]; then
    cp /vagrant/conf/template.env /vagrant/.env
fi
sudo ln -sf /vagrant/conf/.bashrc /home/vagrant/.bashrc

# Install Python dependencies
export PATH=$PATH:/home/vagrant/.local/bin
export PIPENV_VENV_IN_PROJECT=true
export PIP_NO_BINARY=psycopg2
cd /vagrant
pipenv install --dev

# Init application
pipenv run python /vagrant/manage.py migrate --noinput
pipenv run python /vagrant/manage.py collectstatic --noinput

# Run application as a service
sudo ln -sf /vagrant/conf/systemd/django.service /etc/systemd/system/.
sudo systemctl daemon-reload
sudo systemctl enable django
sudo systemctl start django
sudo systemctl restart nginx
